1. Clone the repository, 
  
  git clone https://gitlab.eurecom.fr/oai/docker-image-openaircn.git

2. Load the repository (assuming docker is already installed)
  
  cd docker-image-openair-cn
  
  docker load -i oai_epc.tar
  
3. Mysql root password <linux>